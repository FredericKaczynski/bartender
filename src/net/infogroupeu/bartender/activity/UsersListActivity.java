package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.adapter.UserAdapter;
import net.infogroupeu.bartender.model.User;

import java.util.Arrays;

public class UsersListActivity extends Activity {

    UserAdapter adapter;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_users_list);
    }

        public void onResume(){
            super.onResume();

            ListView listView = (ListView) this.findViewById(R.id.users_list_view);

            final User[] users = User.getAll();

            adapter = new UserAdapter(this, R.layout.users_list_element, Arrays.asList(users));

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(UsersListActivity.this, UserShowActivity.class);
                    intent.putExtra("userLogin", users[position].getLogin());
                    startActivity(intent);
                }

            });
    }



}

