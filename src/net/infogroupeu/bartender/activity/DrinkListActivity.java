package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import net.infogroupeu.bartender.BartenderApplication;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.adapter.DrinkAdapter;
import net.infogroupeu.bartender.model.Drink;

import java.util.Arrays;

public class DrinkListActivity extends Activity {

    private BartenderApplication application;
    private Drink[] drinks;
    private int orderId;

    @Override
    protected void onCreate(Bundle savedInstancedState) {
        super.onCreate(savedInstancedState);
        this.setContentView(R.layout.activity_carte);

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) this.findViewById(R.id.search_drink);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);

        this.application = BartenderApplication.getInstance();

        Intent intent = getIntent();
        this.drinks = null;
        // On recupere la liste de toutes les boissons, et on cree l'adapteur pour la ListView
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null && !query.isEmpty()) {
                this.drinks = Drink.search(query);
                searchView.setQueryHint(query);
            }
        }



        // Il se peut que nous recevions un orderId venant de NewOrderActivity
        this.orderId = intent.getIntExtra("orderId", 0);
        if(orderId!=0){
            searchView.setVisibility(View.GONE);
        }
        if (this.drinks == null)
            this.drinks = Drink.getAll();

        DrinkAdapter adapter = new DrinkAdapter(this, android.R.layout.simple_list_item_1, Arrays.asList(this.drinks));

        ListView listView = (ListView) this.findViewById(R.id.drink_list);

        listView.setAdapter(adapter);

        final Context context = this;
        final int orderId = this.orderId;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(context, ShowDrinkActivity.class);
                intent.putExtra("drinkId", drinks[position].getId());
                intent.putExtra("orderId", orderId);
                startActivity(intent);

            }
        });

    }
}
