package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import net.infogroupeu.bartender.BartenderApplication;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.adapter.DrinkAdapter;
import net.infogroupeu.bartender.adapter.StockAdapter;
import net.infogroupeu.bartender.model.Drink;

import java.util.Arrays;

/**
 * Created by kenzo on 07/05/15.
 */
public class StockActivity extends Activity {

    private BartenderApplication application;
    private Drink[] drinks;

    @Override
    protected void onCreate(Bundle savedInstancedState) {
        super.onCreate(savedInstancedState);
        this.setContentView(R.layout.activity_stock);


    }

    @Override
    public void onResume() {
        super.onResume();
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        SearchView searchView = (SearchView) this.findViewById(R.id.search_drink_stock);
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        int id = searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);

       this.application = BartenderApplication.getInstance();

        Intent intent = getIntent();
        this.drinks = null;

        // On recupere la liste de toutes les boissons, et on cree l'adapteur pour la ListView
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null && !query.isEmpty()) {
                this.drinks = Drink.search(query);
                searchView.setQueryHint(query);
            }
        }

        if (this.drinks == null)
            this.drinks = Drink.getAll();


        StockAdapter adapter = new StockAdapter(this, R.layout.activity_stock_element, Arrays.asList(this.drinks));

        ListView listView = (ListView) this.findViewById(R.id.stock_list);

        listView.setAdapter(adapter);

        final Context context = this;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                updateStockDialog(drinks[position]);
            }
        });
    }

    public void updateStockDialog(Drink drink) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_stock);
        dialog.setTitle(getString(R.string.update_stock));
        final Drink drink2 = drink; //hack moche
        EditText edit = (EditText) dialog.findViewById(R.id.current_stock);
        edit.setText(Integer.toString(drink.getStock()));

        Button on = (Button)dialog.findViewById(R.id.stock_button);
        on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit = (EditText) dialog.findViewById(R.id.current_stock);
                int stock;

                if(edit.getText().toString().matches("")) {

                    stock = 0;

                }else{
                    stock = Integer.parseInt(edit.getText().toString());
                }
                if (stock <= drink2.getMax())
                    drink2.setStock(stock);
                dialog.dismiss();
                onResume();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }
}
