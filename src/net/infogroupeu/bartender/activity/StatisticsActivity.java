package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.*;

import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.adapter.DrinkAdapter;
import net.infogroupeu.bartender.model.Order;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.ITouchHandler;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.tools.PanListener;
import org.achartengine.tools.ZoomListener;

import java.util.Arrays;

/**
 * Created by nico_kwak on 07-05-15.
 */
public class StatisticsActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_statistics);
        String revenueByWaiters [] = Order.getRevenueByWaiter();
        Double revenueByDays [] = Order.revenueForDays();
        String quantityByDrink[] = Order.cumulatedDrinksSells();


        ArrayAdapter<String> adapter = new ArrayAdapter(StatisticsActivity.this, android.R.layout.simple_list_item_1, Arrays.asList(revenueByWaiters));
        ListView listView = (ListView) (findViewById(R.id.waiter_revenue_list));
        listView.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter(StatisticsActivity.this, android.R.layout.simple_list_item_1, Arrays.asList(quantityByDrink));
        ListView listView2 = (ListView) (findViewById(R.id.drink_revenue_list));
        listView2.setAdapter(adapter2);

        // Initialisation du graphique
        // La view qui va contenir notre graphique
        LinearLayout layout = (LinearLayout) findViewById(R.id.chart_revenue);

        XYSeriesRenderer renderer = new XYSeriesRenderer();
        XYMultipleSeriesRenderer multipleRenderer = new XYMultipleSeriesRenderer(10);
        multipleRenderer.addSeriesRenderer(renderer);
        renderer.setLineWidth(5f);
        renderer.setColor(this.getResources().getColor(R.color.brown));

        multipleRenderer.setApplyBackgroundColor(true);
        multipleRenderer.setBackgroundColor(this.getResources().getColor(R.color.pale_white));
        multipleRenderer.setMarginsColor(this.getResources().getColor(R.color.brown));
        multipleRenderer.setLabelsTextSize(40);
        multipleRenderer.setLegendTextSize(50);
        multipleRenderer.setYLabelsPadding(30);
        multipleRenderer.setZoomEnabled(false, false);
        multipleRenderer.setPanEnabled(false, false);


        XYSeries dataset = new XYSeries("");
        XYMultipleSeriesDataset multipleDataset = new XYMultipleSeriesDataset();
        multipleDataset.addSeries(dataset);

        int i;

        for(i = 0; revenueByDays.length>i; i++){

            dataset.add(i, revenueByDays[i]);

        }


        GraphicalView chartView = ChartFactory.getLineChartView(this, multipleDataset, multipleRenderer);

        layout.addView(chartView);


        LinearLayout layout2 = (LinearLayout) findViewById(R.id.chart_cumulated_revenue);

        XYSeriesRenderer renderer2 = new XYSeriesRenderer();
        XYMultipleSeriesRenderer multipleRenderer2 = new XYMultipleSeriesRenderer(10);
        multipleRenderer2.addSeriesRenderer(renderer2);
        renderer2.setLineWidth(5f);
        renderer2.setColor(this.getResources().getColor(R.color.brown));

        multipleRenderer2.setApplyBackgroundColor(true);
        multipleRenderer2.setBackgroundColor(this.getResources().getColor(R.color.pale_white));
        multipleRenderer2.setMarginsColor(this.getResources().getColor(R.color.brown));
        multipleRenderer2.setLabelsTextSize(40);
        multipleRenderer2.setLegendTextSize(50);
        multipleRenderer2.setYLabelsPadding(30);
        multipleRenderer2.setZoomEnabled(false, false);
        multipleRenderer2.setPanEnabled(false, false);


        XYSeries dataset2 = new XYSeries("");
        XYMultipleSeriesDataset multipleDataset2 = new XYMultipleSeriesDataset();
        multipleDataset2.addSeries(dataset2);

        double cumulatedRevenue=0;

        for(i = 0; revenueByDays.length>i; i++){
            cumulatedRevenue = cumulatedRevenue + revenueByDays[i];
            dataset2.add(i, cumulatedRevenue);

        }


        GraphicalView chartView2 = ChartFactory.getLineChartView(this, multipleDataset2, multipleRenderer2);
        layout2.addView(chartView2);
    }
}
