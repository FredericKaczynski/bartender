package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.User;

/**
 * Created by nico_kwak on 01-05-15.
 */

public class UserShowActivity extends Activity {

    private EditText loginInput;
    private EditText passwordInput;

    User user;

    int userType;
    String newPassword;
    String newLogin;

    boolean modified = false;
    boolean passwordModified = false;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_user_show);

        ArrayAdapter <CharSequence> adapter;

        String userLogin = getIntent().getStringExtra("userLogin");
        user = User.getUserByLogin(userLogin);


        this.loginInput =(EditText)findViewById(R.id.login_input_user_show);
        this.passwordInput = (EditText) findViewById(R.id.password_input_user_show);

        loginInput.setText(userLogin);
        passwordInput.setText(user.getPassword());


        Spinner spinner = (Spinner) findViewById(R.id.spinner_user_type);
        adapter = ArrayAdapter.createFromResource(this, R.array.login_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(adapter);

        spinner.setSelection(User.getUserByLogin(userLogin).getType()-1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    userType = position+1;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                    userType = user.getType();
            }

        });
    }


        public void cancel(View view){

            Intent intent = new Intent(UserShowActivity.this, UsersListActivity.class);
            intent.setFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP));
            startActivity(intent);

        }

        public void confirm (View view) {

            final EditText confirmPassword = new EditText(this);
            newPassword = this.passwordInput.getText().toString();
            newLogin = this.loginInput.getText().toString();
            boolean OK = true;

            AlertDialog dialog3;
            AlertDialog.Builder builder3 = new AlertDialog.Builder(UserShowActivity.this);

            if(newPassword.length() < 6 || newPassword.length() > 16){

                builder3.setTitle(getString(R.string.password_too_short));
                builder3.setMessage(getString(R.string.password_size));
                builder3.setPositiveButton("OK", null);

                dialog3 = builder3.create();
                dialog3.show();
                OK = false;
            }

            if(newLogin.length()<=5 || newLogin.length()>=17) {

                builder3.setTitle(getString(R.string.login_too_short));
                builder3.setMessage(getString(R.string.login_size));
                builder3.setPositiveButton("OK", null);

                dialog3 = builder3.create();
                dialog3.show();
                OK = false;

            }
            if(User.getUserByLogin(newLogin)!= null && !newLogin.equals(user.getLogin())){

                builder3.setTitle(getString(R.string.login_used));
                builder3.setMessage(getString(R.string.login_already_used));
                builder3.setPositiveButton("OK", null);

                dialog3 = builder3.create();
                dialog3.show();
                OK = false;

            }
            if (!newLogin.equals(user.getLogin()) && OK) {

                user.setLogin(loginInput.getText().toString());

            }
            if (userType != user.getType() && OK) {

                user.setType(userType);

            }
            if (OK) {
                user.setPassword(newPassword);
                User.update(user);

            }
            if (OK) {
                Toast.makeText(UserShowActivity.this, getString(R.string.user_updated), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(UserShowActivity.this, UsersListActivity.class);
                intent.setFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP));
                startActivity(intent);
            }

        }
}
