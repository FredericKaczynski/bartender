package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import net.infogroupeu.bartender.BartenderApplication;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.adapter.DrinkAdapter;
import net.infogroupeu.bartender.adapter.DrinkOrderAdapter;
import net.infogroupeu.bartender.adapter.OrderAdapter;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.Order;
import net.infogroupeu.bartender.model.OrderLine;
import net.infogroupeu.bartender.model.User;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

public class OrderShowActivity extends Activity {

    private BartenderApplication application;
    Order order[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_order_show);

        this.application = BartenderApplication.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.order = Order.getAll();
        Collections.reverse(Arrays.asList(this.order));

        OrderAdapter adapter = new OrderAdapter(this, R.layout.activity_order_element, Arrays.asList(this.order));
        ListView listView = (ListView) this.findViewById(R.id.order_List);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(OrderShowActivity.this, NewOrderActivity.class);
                System.out.println(position);
                intent.putExtra("orderId", order[position].getId());
                startActivity(intent);
            }
        });

    }

    public void addOrderDialog(View view) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_table);
        dialog.setTitle(getString(R.string.table_number));

        Button on = (Button)dialog.findViewById(R.id.table_button);
        on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit = (EditText) dialog.findViewById(R.id.table_number);
                int table = Integer.parseInt(edit.getText().toString());
                addOrder(table);
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void addOrder(int table) {
        User connectedUser = BartenderApplication.getInstance().getConnectedUser();
        EditText edit = (EditText) this.findViewById(R.id.table_number);
        Order order = Order.create(connectedUser, table, new Date());

        Intent intent = new Intent(this, NewOrderActivity.class);
        intent.putExtra("orderId", order.getId());
        startActivity(intent);

    }
}
