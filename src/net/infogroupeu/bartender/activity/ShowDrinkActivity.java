package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import net.infogroupeu.bartender.BartenderApplication;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.Order;
import net.infogroupeu.bartender.model.OrderLine;
import net.infogroupeu.bartender.model.User;
import net.infogroupeu.bartender.model.Vote;


public class ShowDrinkActivity extends Activity {

    Vote vote;
    User user = BartenderApplication.getInstance().getConnectedUser();
    Drink drink;
    Order order;

    RatingBar ratingBar;

    float voteValue;
    int drinkId;

    boolean hasVoted = false;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_show_drink);

        Intent intent = getIntent();
        drinkId = intent.getIntExtra("drinkId", 0);
        drink = Drink.getById(drinkId);

        NumberPicker numberPicker = (NumberPicker) this.findViewById(R.id.setNbDrinks);
        TextView usefullData = (TextView) findViewById(R.id.usefullData);

        int orderId = intent.getIntExtra("orderId", 0);
        final Order order = Order.getById(orderId);
        this.order = order;
        if (this.order == null) {

            if (drink.getQuantite() == null) { drink.setQuantite(" ");}
            if (drink.getAlcoolemie() == null) { drink.setAlcoolemie("0");}
            usefullData.setText(getString(R.string.alcohol_volume)+" : \n"+drink.getAlcoolemie()+"%" + "\n"+getString(R.string.quantity)+" : \n" + drink.getQuantite());

            numberPicker.setVisibility(View.GONE);
        }
        else {
            usefullData.setVisibility(View.GONE);
            numberPicker.setMinValue(0);
            numberPicker.setMaxValue(drink.getStock());
            OrderLine line = OrderLine.getByOrderAndDrink(order, drink);
            if (line != null) {
                numberPicker.setMaxValue(drink.getStock() + line.getQuantity());
                numberPicker.setValue(line.getQuantity());
            }
        }

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                order.setDrinkQuantity(drink, newVal);

            }
        });

        TextView drinkDescription = (TextView) findViewById(R.id.drinkDescription);
        drinkDescription.setText(drink.getDescription());

        TextView drinkName = (TextView) findViewById(R.id.drinkName);
        drinkName.setText(drink.getName());

        Drawable photo = drink.getPhoto(this);
        if (photo != null) {
            ImageView imageView = (ImageView) findViewById(R.id.showDrinkPhoto);
            imageView.setImageDrawable(photo);
        }


        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setEnabled(true);
        ratingBar.setMax(5);
        ratingBar.setStepSize(0.01f);
        ratingBar.setRating((float) drink.getMeanNote());

        user = BartenderApplication.getInstance().getConnectedUser();
        hasVoted = Vote.hasVoted(drink.getId(), user.getId());
        if (hasVoted) {
            ratingBar.setEnabled(false);
        } else {

            ratingBar.setEnabled(true);
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                    voteValue = rating;

                }
            });
        }
    }


    public void vote(View view) {

        if (hasVoted) {

            Toast.makeText(ShowDrinkActivity.this, getString(R.string.already_voted), Toast.LENGTH_SHORT).show();

        }else {


            Vote.insert(drink.getId(), (double) voteValue, user.getId());

            ratingBar.setRating((float) drink.getMeanNote());
            ratingBar.setEnabled(false);

            hasVoted = true;

            Toast.makeText(ShowDrinkActivity.this, getString(R.string.thanks_vote), Toast.LENGTH_SHORT).show();
        }
    }

    public void addToOrder(View view) {
        Order order = Order.getCurrentOrder(user);
        NumberPicker picker = (NumberPicker) findViewById(R.id.setNbDrinks);

        if (order != null && picker.getValue() > 0) {
            order.setDrinkQuantity(this.drink, picker.getValue());
            this.finish(); //on a ajouté, activité finie, basta
        }
    }

}
