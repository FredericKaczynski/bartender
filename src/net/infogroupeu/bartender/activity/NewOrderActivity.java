package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.adapter.DrinkOrderAdapter;
import net.infogroupeu.bartender.adapter.OrderAdapter;
import net.infogroupeu.bartender.model.Order;
import net.infogroupeu.bartender.model.OrderLine;

import java.util.Arrays;

public class NewOrderActivity extends Activity{

    private Order order;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_order_show_new);

        int orderId = this.getIntent().getIntExtra("orderId", 0);
        Order order = Order.getById(orderId);
        this.order = order;
        TextView table_id = (TextView) findViewById(R.id.table_id);
        table_id.setText(getString(R.string.table) + " : "+Integer.toString(order.getTableNumber()));
    }

    @Override
     public void onResume() {
        super.onResume();
        final OrderLine lines[] = OrderLine.getOrderLines(order);

        DrinkOrderAdapter adapter = new DrinkOrderAdapter(this, R.layout.activity_order_line, Arrays.asList(lines));

        listView = (ListView) this.findViewById(R.id.order_line);

        listView.setAdapter(adapter);

        final Context context = this;
        if (!order.hasPaid()) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(context, ShowDrinkActivity.class);
                    intent.putExtra("drinkId", lines[position].getDrink().getId());
                    intent.putExtra("orderId", order.getId());
                    startActivity(intent);
                }
            });
        }

        TextView totalText = (TextView) findViewById(R.id.order_sum);
        totalText.setText(Float.toString(order.getTotal()) + " \u20ac");

        if (order.hasPaid()) {
            LinearLayout actions = (LinearLayout) findViewById(R.id.actions_box);
            actions.setVisibility(View.GONE);
        }
    }

    public void toDrinksList(View view) {

        // On va sur l'activite DrinkList en transmettant l'id de la commande
        Intent intent = new Intent(this, DrinkListActivity.class);

        intent.putExtra("orderId", this.order.getId());

        this.startActivity(intent);

    }

    public void payOrder(View view) {
        order.pay();
        listView.setOnItemClickListener(null);
        onResume();
    }
}
