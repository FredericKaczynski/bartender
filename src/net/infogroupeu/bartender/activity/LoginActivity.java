package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import net.infogroupeu.bartender.BartenderApplication;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.User;

public class LoginActivity extends Activity {

    private EditText loginInput;
    private EditText passwordInput;

    private BartenderApplication application;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_login);

        /**
         * We initialize the application
         */
        this.application = BartenderApplication.getInstance();

        this.loginInput = (EditText) this.findViewById(R.id.login_login_input);
        this.passwordInput = (EditText) this.findViewById(R.id.login_password_input);
        System.out.println(this.loginInput);


        // A ENLEVER APRES
        this.loginInput.setText("owner1");
        this.passwordInput.setText("owner");
        // FIN ENLEVER


    }

    public void attemptLogin(View view) {

        String login = this.loginInput.getText().toString();
        String password = this.passwordInput.getText().toString();

        // TODO: Vérifier que les identifiants sont bons
        if (this.application.loginUser(login, password) != null) {

            Intent intent = new Intent(this, MenuActivity.class);
            Intent intent2 = new Intent(this, DrinkListActivity.class);

            User user = this.application.getConnectedUser();
            if (user.getType() == user.CLIENT) { this.startActivity(intent2);}
            else {this.startActivity(intent);}

        } else {

            Toast.makeText(LoginActivity.this,getString(R.string.invalid_password), Toast.LENGTH_SHORT).show();

        }

    }
    public void subscribe(View view){
        Intent intent = new Intent (this, SubscribeActivity.class);
        startActivity(intent);
    }
}
