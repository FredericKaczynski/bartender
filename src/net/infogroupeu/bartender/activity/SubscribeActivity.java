package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.User;


public class SubscribeActivity extends Activity {

    private EditText login;
    private EditText password;
    private EditText confirmPassword;

    public void onCreate(Bundle savedInstanceSTate) {

        super.onCreate(savedInstanceSTate);
        this.setContentView(R.layout.activity_subscribe);
        this.login=(EditText) findViewById(R.id.loginInput);

        this.password = (EditText) findViewById(R.id.passwordInput);
        this.confirmPassword = (EditText) findViewById(R.id.confirmPasswordInput);
    }

    public void attemptSubscribe(View view){

        String login = this.login.getText().toString();
        String password = this.password.getText().toString();
        String confirmPassword = this.confirmPassword.getText().toString();

        User user = User.getUserByLogin(login);

        if (user == null) {
            AlertDialog dialog3;
            AlertDialog.Builder builder3 = new AlertDialog.Builder(SubscribeActivity.this);

            if(password.length() < 6 || password.length() > 16){

                builder3.setTitle(getString(R.string.password_too_short));
                builder3.setMessage(getString(R.string.password_size));
                builder3.setPositiveButton("OK", null);

                dialog3 = builder3.create();
                dialog3.show();
            }
            else if(login.length() < 6 || login.length() > 16) {

                builder3.setTitle(getString(R.string.login_too_short));
                builder3.setMessage(getString(R.string.login_size));
                builder3.setPositiveButton("OK", null);

                dialog3 = builder3.create();
                dialog3.show();

            }
            else if(!password.equals(confirmPassword) ){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(getString(R.string.password_match) + "\n \n \n");
                builder.setPositiveButton("OK", null);

                AlertDialog alert = builder.create();

                alert.show();
            } else {

                User.insert(login, password, User.CLIENT);

                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, getString(R.string.account_created), duration);
                toast.show();

                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags((Intent.FLAG_ACTIVITY_CLEAR_TOP));
                startActivity(intent);
            }

        } else {

            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);

            builder2.setTitle(getString(R.string.login_used));
            builder2.setMessage(getString(R.string.login_used2));
            builder2.setPositiveButton("OK",null);
            AlertDialog alert = builder2.create();

            alert.show();
        }
    }
}
