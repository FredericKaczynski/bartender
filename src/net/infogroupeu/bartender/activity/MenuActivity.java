package net.infogroupeu.bartender.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import net.infogroupeu.bartender.BartenderApplication;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.User;

public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_menu);

        User connectedUser = BartenderApplication.getInstance().getConnectedUser();

        Button buttonOrder = (Button) this.findViewById(R.id.button_order);
        Button buttonStock = (Button) this.findViewById(R.id.button_stock);
        Button buttonUserList = (Button) this.findViewById(R.id.button_user_list);
        Button buttonStatictics = (Button) this.findViewById(R.id.stats);

        // On cache les bouttons selon les types d'utilisateurs
        /*if (connectedUser.getType() == User.CLIENT) {

            buttonOrder.setVisibility(View.GONE);
            buttonStock.setVisibility(View.GONE);
            buttonUserList.setVisibility(View.GONE);
            buttonFinance.setVisibility(View.GONE);
        */


        if (connectedUser.getType() == User.WAITER) {

            buttonStatictics.setVisibility(View.GONE);
            buttonUserList.setVisibility(View.GONE);

        } else if (connectedUser.getType() == User.OWNER) {

            // Tout est affiché !

        }


    }

    public void openDrinksList(View view) {

        Intent intent = new Intent(this, DrinkListActivity.class);

        this.startActivity(intent);

    }

    public void openShowOrder(View view) {

        Intent intent = new Intent(this, OrderShowActivity.class);

        this.startActivity(intent);

    }

    public void openStock(View view) {

        Intent intent = new Intent(this, StockActivity.class);

        this.startActivity(intent);

    }

    public void openUserList(View view){

        Intent intent = new Intent(this, UsersListActivity.class);
        startActivity(intent);
    }

    public void openStatistics(View view){

        Intent intent = new Intent(this, StatisticsActivity.class);
        startActivity(intent);

    }
}
