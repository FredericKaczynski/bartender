package net.infogroupeu.bartender.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.OrderLine;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DrinkOrderAdapter extends ArrayAdapter<OrderLine> {

    private List<OrderLine> items;
    private Context context;

    public DrinkOrderAdapter(Context context, int textViewResourceId, List<OrderLine> items) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Va chercher la ligne de commande que cette vue doit afficher dans la liste
        OrderLine orderLine = this.items.get(position);

        // On genere une vue "vierge"
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_order_line, parent, false);
        }

        // On remplit la vue avec les informations de la ligne de commande
        TextView nameTextView = (TextView) convertView.findViewById(R.id.drinkOrderName);
        nameTextView.setText(orderLine.getDrink().getName());

        TextView quantityTextView = (TextView) convertView.findViewById(R.id.drinkorderQuantity);
        quantityTextView.setText(Integer.toString(orderLine.getQuantity())+ " x");

        TextView priceTextView = (TextView) convertView.findViewById(R.id.drinkOrderPrice);
        priceTextView.setText(Float.toString(orderLine.getQuantity() * orderLine.getDrink().getPrice()) + " €");

        return convertView;

    }

}