package net.infogroupeu.bartender.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DrinkAdapter extends ArrayAdapter<Drink> {

    private List<Drink> items;
    private Context context;

    public DrinkAdapter(Context context, int textViewResourceId, List<Drink> items) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Va chercher la boisson que cette vue doit afficher dans la liste
        Drink drink = this.items.get(position);

        // On genere une vue "vierge"
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_carte_element, parent, false);
        }

        // On remplit la vue avec les informations de la boissons
        TextView nameTextView = (TextView) convertView.findViewById(R.id.drinkName);
        nameTextView.setText(drink.getName());

        TextView priceTextView = (TextView) convertView.findViewById(R.id.drinkPrice);
        priceTextView.setText(Float.toString(drink.getPrice()) + "\u20ac"); //drink.getName());

        Drawable icon = drink.getIcon(this.context);
        if (icon != null) {
            ImageView imageView = (ImageView) convertView.findViewById(R.id.drinkIcon);
            imageView.setImageDrawable(icon);
        }


        return convertView;

    }

}
