package net.infogroupeu.bartender.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;

import java.util.List;

/**
 * Created by kenzo on 07/05/15.
 */
public class StockAdapter extends ArrayAdapter<Drink> {
    private List<Drink> items;
    private Context context;

    public StockAdapter(Context context, int textViewResourceId, List<Drink> items) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Va chercher la boisson que cette vue doit afficher dans la liste
        Drink drink = this.items.get(position);

        // On genere une vue "vierge"
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_stock_element, parent, false);
        }

        // On remplit la vue avec les informations de la boissons
        TextView nameTextView = (TextView) convertView.findViewById(R.id.drinkName);
        nameTextView.setText(drink.getName());

        TextView seuilTextView = (TextView) convertView.findViewById(R.id.drinkSeuil);
        seuilTextView.setText(Integer.toString(drink.getSeuil()));

        TextView stockTextView = (TextView) convertView.findViewById(R.id.drinkStock);
        stockTextView.setText(Integer.toString(drink.getStock()));

        /*
        Drawable photo = drink.getPhoto(this.context);
        if (photo != null) {
            ImageView imageView = (ImageView) convertView.findViewById(R.id.drinkPhoto);
            imageView.setImageDrawable(photo);
        }
        */

        Drawable icon = drink.getIcon(this.context);
        if (icon != null) {
            ImageView imageView = (ImageView) convertView.findViewById(R.id.drinkIcon);
            imageView.setImageDrawable(icon);
        }
        if (drink.getStock() > (drink.getSeuil())){
            ImageView attention = (ImageView) convertView.findViewById(R.id.attention);
            attention.setVisibility(View.GONE);
        }
        return convertView;

    }

}
