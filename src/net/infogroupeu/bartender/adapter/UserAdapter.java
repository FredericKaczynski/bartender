package net.infogroupeu.bartender.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.User;

import java.util.List;

public class UserAdapter extends ArrayAdapter<User> {
    private List<User> items;
    private Context context;

    public UserAdapter(Context context, int textViewResourceId, List<User> items) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Va chercher la boisson que cette vue doit afficher dans la liste
        User user = this.items.get(position);

        // On genere une vue "vierge"
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.users_list_element, parent, false);
        }

        // On remplit la vue avec les informations de la boissons
        TextView nameTextView = (TextView) convertView.findViewById(R.id.user_login);
        nameTextView.setText(user.getLogin());

        return convertView;

    }

}
