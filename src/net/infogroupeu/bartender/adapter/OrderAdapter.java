package net.infogroupeu.bartender.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.infogroupeu.bartender.R;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.Order;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by twyckmans on 7/05/15.
 */
public class OrderAdapter extends ArrayAdapter{
    private List<Order> items;
    private Context context;

    public OrderAdapter(Context context, int textViewResourceId, List<Order> items) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Va chercher la commande que cette vue doit afficher dans la liste
        Order order = this.items.get(position);

        // On genere une vue "vierge"
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_order_element, parent, false);
        }

        Drawable icon = order.getIcon(this.context);
        ImageView imagePaye = (ImageView) convertView.findViewById(R.id.image_Paye);
        imagePaye.setImageDrawable(icon);

        TextView tableTextView = (TextView) convertView.findViewById(R.id.order_table);
        tableTextView.setText(context.getString(R.string.table)+ " " + Integer.toString(order.getTableNumber()));

        TextView waiterTextView = (TextView) convertView.findViewById(R.id.order_waiter);
        waiterTextView.setText(order.getWaiter().getLogin());


        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm");
        TextView dateTextView = (TextView) convertView.findViewById(R.id.order_date);
        dateTextView.setText(df.format(order.getDate()));

        return convertView;

    }
}
