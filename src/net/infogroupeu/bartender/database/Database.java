package net.infogroupeu.bartender.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import net.infogroupeu.bartender.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Database extends SQLiteOpenHelper {

    private Context context;

    public Database(Context context, String name) {
        super(context, name, null, 1);

        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.initSchema(db);
    }

    public void initSchema(SQLiteDatabase db) {

        // On ouvre le fichier contenant les requetes de creation de base de donnees
        // Ce fichier se situe dans res/raw/database_create.sql
        InputStream databaseCreateFile = context.getResources().openRawResource(R.raw.database_create);
        BufferedReader br = new BufferedReader(new InputStreamReader(databaseCreateFile));

        String createQuery = "";
        String readLine = "";

        try {

            while ((readLine = br.readLine()) != null) {

                // On part du principe que chaque requete est separe par une ligne vide
                // Cela permet d'ecrire les requetes sur plusieurs lignes

                if (readLine.isEmpty()) {

                    Log.d("SQL Creation Query", createQuery);
                    db.execSQL(createQuery);
                    createQuery = "";

                } else {

                    createQuery += readLine.replace("\n", "");

                }

            }

            if (!createQuery.isEmpty()) {
                // On execute la derniere ligne trouve
                Log.d("SQL Creation Query", createQuery);
            }

            db.execSQL(createQuery);

            br.close();

        } catch (IOException e) {
            Log.e("bartender", "Erreur lors de l'initialisation de la BDD", e);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        this.deleteDatabase(db);
        this.initSchema(db);

    }

    private void deleteDatabase(SQLiteDatabase db) {
        Cursor c = db.query("sqlite_master", new String[]{"name"}, "type = 'table' AND name NOT LIKE '%sqlite_%'", null, null, null, null, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            db.execSQL("DROP TABLE IF EXISTS " + c.getString(0));
            c.moveToNext();
        }
    }

    public SQLiteDatabase getWritable() {

        return this.getWritableDatabase();

    }

    public SQLiteDatabase getReadable() {

        return this.getReadableDatabase();

    }
}
