package net.infogroupeu.bartender.model;

import android.database.Cursor;
import android.content.ContentValues;
import android.util.Log;
import android.view.View;

import net.infogroupeu.bartender.database.Database;

public class Vote {

    int boissonId;
    private double note;
    private int userId;


    private static Database database;
    public static String table = "vote";

    public Vote( int boissonId, double note, int userId) {
        this.boissonId = boissonId;
        this.note = note;
        this.userId = userId;
    }

    public static boolean hasVoted(int drinkId, int userId) {
        Cursor data = Vote.database.getReadable().rawQuery("SELECT * FROM " + Vote.table + " WHERE boisson_id = ? AND user_id =?;", new String[]{Integer.toString(drinkId), Integer.toString(userId)});
        data.moveToFirst();
        if(data.getCount()==0){

            return false;
        }
        return true;
    }

    private static Vote craft(Cursor data) {

        if (data.getCount() == 0)
            return null;

        return new Vote(
                data.getInt(data.getColumnIndex("boisson_id")),
                data.getDouble(data.getColumnIndex("note")),
                data.getInt(data.getColumnIndex("user_id"))
        );

    }

    public static Vote insert(int boissonId, double note, int userId) {

        ContentValues values = new ContentValues();
        values.put("boisson_id", boissonId);
        values.put("note", note);
        values.put("user_id", userId);

        int passed = (int) Vote.database.getWritable().insert(Vote.table, null, values);

        if (passed == -1) {
            return null;
        }

        return new Vote(
                boissonId,
                note,
                userId
        );
    }

    public static void init(Database database){
        Vote.database = database;
    }
}