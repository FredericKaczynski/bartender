package net.infogroupeu.bartender.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.util.Log;

import net.infogroupeu.bartender.database.Database;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Order {

    private int id;
    private Date dateCreate;
    private User waiter;
    private int tableNumber;
    private boolean paid;

    public Order(int id, Date dateCreate, User waiter, int tableNumber, boolean paid) {
        this.id = id;
        this.dateCreate = dateCreate;
        this.waiter = waiter;
        this.tableNumber = tableNumber;
        this.paid = paid;
    }

    public float getTotal() {
        float total = 0;
        OrderLine lines[] = OrderLine.getOrderLines(this);

        for(int i=0; i < lines.length; i++) {
            total = total + (lines[i].getQuantity() * lines[i].getDrink().getPrice());
        }

        return total;
    }

    public void pay() {
        if (paid == false) {
            ContentValues values = new ContentValues();

            values.put("paid", 1);

            String whereClause = "order_id = ?";

            database.getWritable().update(
                    table,
                    values,
                    whereClause,
                    new String[] {
                            Integer.toString(getId()),
                    }
            );
            paid = true;
        }
    }

    public Date getDate() {
        return dateCreate;
    }

    public boolean hasPaid() {
        return paid;
    }

    public void setDrinkQuantity(Drink drink, int quantity) {

        // On regarde d'abord si une entrée existe déjà dans la BDD
        OrderLine orderLine = OrderLine.getByOrderAndDrink(this, drink);


        if (orderLine != null) {
            // Il suffit d'update l'entree
            orderLine.setQuantity(quantity);
            OrderLine.update(orderLine);

        } else {
            drink.setStock(drink.getStock() - quantity);
            ContentValues values = new ContentValues();
            values.put("drink_id", drink.getId());
            values.put("order_id", this.getId());
            values.put("quantity", quantity);

            int newId = (int) Order.database.getWritable().insert(OrderLine.table, null, values);

        }

    }



    public int getTableNumber() {return tableNumber; }
    public User getWaiter() {return waiter; }
    public int getId() {
        return id;
    }

    /**
     * Static members
     */

    private static Database database;
    public static String table = "\"order\"";

    public static void init(Database database) {
        Order.database = database;
    }

    public static Order[] getAll() {

        return Order.craftMultiple(Order.database.getReadable()
                .rawQuery("SELECT * FROM " + Order.table + ";", null));

    }

    public static Order getCurrentOrder(User waiter) {
        Cursor data = Order.database.getReadable()
                .rawQuery("SELECT * FROM " + Order.table + " WHERE waiter_user_id = ?",
                        new String[]{Integer.toString(waiter.getId())});

        data.moveToFirst();

        return Order.craft(data);

    }

    public static Order getById(int id) {
        Cursor data = Order.database.getReadable()
                .rawQuery("SELECT * FROM " + Order.table + " WHERE order_id = ?;", new String[]{Integer.toString(id)});

        data.moveToFirst();

        return Order.craft(data);

    }

    public static Order create(User waiter, int tableNumber, Date date) {

        ContentValues values = new ContentValues();

        values.put("waiter_user_id", waiter.getId());
        values.put("table_num", tableNumber);
        values.put("date_create", System.currentTimeMillis());

        int newId = (int) Order.database.getWritable().insert(Order.table, null, values);

        Order order = new Order(
                newId,
                date,
                waiter,
                tableNumber,
                false
        );

        return order;
    }

    private static Order[] craftMultiple(Cursor data) {

        Order[] orders = new Order[data.getCount()];

        while (data.moveToNext()) {

            Order order = Order.craft(data);
            if (orders != null) {
                orders[data.getPosition()] = order;
            }
        }

        return orders;

    }

    public static Order craft(Cursor data) {

        if (data.getCount() == 0)
            return null;

        return new Order(
            data.getInt(data.getColumnIndex("order_id")),
            new Date(data.getLong(data.getColumnIndex("date_create"))),
            User.getUserById(data.getInt(data.getColumnIndex("waiter_user_id"))),
            data.getInt(data.getColumnIndex("table_num")),
                data.getInt(data.getColumnIndex("paid")) != 0
        );

    }

    public static Order[] search(String query) {
        return Order.craftMultiple(Order.database.getReadable()
                .rawQuery("SELECT * FROM " + Order.table + " WHERE table_num LIKE '%"+query+"%' COLLATE utf8_general_ci;", null));
    }

    public static Double[] revenueForDays(){

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);

        long time = calendar.getTimeInMillis()/1000;

        Cursor data = Order.database.getReadable()
                .rawQuery("SELECT SUM(B.prix*OL.quantity), O.date_create/86400 as day " +
                        "FROM boisson B, orderline OL, \"order\" O " +
                        "WHERE O.date_create >= ? " +
                        "AND O.order_id = OL.order_id " +
                        "AND OL.drink_id = B.boisson_id " +
                        "GROUP BY day;", new String[]{Long.toString(time)});

        Double[] revenue = new Double[data.getCount()];
        int i = 0;

        while (data.moveToNext()) {

            revenue[i] =data.getDouble(0);
            i++;

        }

        data.moveToFirst();
        return revenue;
    }



    public static String[] getRevenueByWaiter() {

        Cursor data = Order.database.getReadable()
                .rawQuery("SELECT SUM(B.prix*OL.quantity), O.waiter_user_id " +
                        "FROM boisson B, orderline OL, \"order\" O " +
                        "WHERE O.order_id = OL.order_id " +
                        "AND OL.drink_id = B.boisson_id " +
                        "GROUP BY O.waiter_user_id;", null);

        String revenueByWaiter [] = new String[data.getCount()];
        int i = 0;
        User user;

        while (data.moveToNext()) {

            user = User.getUserById(data.getInt(1));
            revenueByWaiter[i] = user.getLogin()+": "+ data.getString(0)+"€";
            i++;

        }

        return revenueByWaiter;
    }

    public static String[] cumulatedDrinksSells(){

        Cursor data = Order.database.getReadable()
                .rawQuery("SELECT SUM(OL.quantity), B.boisson_id " +
                        "FROM boisson B, orderLine OL, \"order\" O " +
                        "WHERE  O.order_id = OL.order_id " +
                        "AND OL.drink_id = B.boisson_id " +
                        "GROUP BY B.boisson_id;", null);

        String cumulatedDrinksSells [] = new String [data.getCount()];
        int i = 0;
        Drink drink;

        while(data.moveToNext()){

            drink = Drink.getById(data.getInt(1));
            cumulatedDrinksSells[i]= drink.getName() + ": " + data.getInt(0);
            i++;

        }

        return cumulatedDrinksSells;
    }



    public Drawable getIcon(Context con) {

        try {
            if (this.paid) {
                    InputStream photo_stream = con.getAssets().open("drinks/photos/v.png");
                    return Drawable.createFromStream(photo_stream, null);
            }
            else {
                InputStream photo_stream = con.getAssets().open("drinks/photos/croix.png");
                return Drawable.createFromStream(photo_stream, null);
            }
        }
        catch (IOException e) {
            return null;
        }
    }
}
