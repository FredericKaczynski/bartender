package net.infogroupeu.bartender.model;

import android.content.ContentValues;
import android.database.Cursor;
import net.infogroupeu.bartender.database.Database;

public class User {

    private int id;
    private String login;
    private String password;
    private int type;
    private int usersAmount;

    public static final int CLIENT = 1;
    public static final int WAITER = 2;
    public static final int OWNER = 3;

    public User(int id, String login, String password, int type) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public int getType() {
        return type;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setType(int type) {
        this.type = type;
    }

    /**
     * Static members
     */

    private static Database database;
    public static String table = "user";

    public static void init(Database database) {
        User.database = database;
    }

    public static User[] getAll() {
        Cursor data = User.database.getReadable().rawQuery("SELECT * FROM " + User.table + ";", null);

        return User.craftMultiple(data);
    }

   static int getAmountUsers() {
        Cursor data = User.database.getReadable().rawQuery("SELECT * FROM " + User.table + ";", null);
        return data.getCount();
    }

    public static User getUserById(int id) {

        Cursor data = User.database.getReadable().rawQuery("SELECT * FROM " + User.table + " WHERE user_id = ?;",
                new String[]{Integer.toString(id)});

        data.moveToFirst();

        return User.craft(data);

    }

    public static User getUserByLogin(String login) {

        Cursor data = User.database.getReadable().rawQuery("SELECT * FROM " + User.table + " WHERE login = ?",
                new String[]{login});

        data.moveToFirst();

        return User.craft(data);
    }

    public static void update(User user) {

        ContentValues values = new ContentValues();

        values.put("login", user.getLogin());
        values.put("password", user.getPassword());
        values.put("type", user.getType());

        String whereClause = "user_id = ?";

        User.database.getWritable().update(User.table, values, whereClause,
                new String[]{Integer.toString(user.getId())});

    }

    public static User insert(String login, String password, int type) {

        ContentValues values = new ContentValues();
        values.put("login", login);
        values.put("password", password);
        values.put("type", type);

        int newId = (int) User.database.getWritable().insert(User.table, null, values);

        if (newId == -1) {
            return null;
        }

        return new User(
                newId,
                login,
                password,
                type
        );

    }

    private static User[] craftMultiple(Cursor data) {

        User[] users = new User[data.getCount()];

        while (data.moveToNext()) {

            User user = User.craft(data);
            if (user != null) {
                users[data.getPosition()] = user;
            }
        }

        return users;

    }

    private static User craft(Cursor data) {

        if (data.getCount() == 0)
            return null;

        return new User(
                data.getInt(data.getColumnIndex("user_id")),
                data.getString(data.getColumnIndex("login")),
                data.getString(data.getColumnIndex("password")),
                data.getInt(data.getColumnIndex("type"))
        );

    }


}
