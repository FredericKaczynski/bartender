package net.infogroupeu.bartender.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;
import android.widget.ArrayAdapter;

import net.infogroupeu.bartender.database.Database;

public class OrderLine{

    private Order order;
    private Drink drink;
    private int quantity;

    public OrderLine(Order order, Drink drink, int quantity) {
        this.order = order;
        this.drink = drink;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public Order getOrder () {
        return order;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setQuantity(int quantity) {
        int diff = quantity - this.quantity;
        this.quantity = quantity;
        this.drink.setStock(this.drink.getStock() - diff);

        this.update(this);
    }

    /**
     * Static members
     */

    private static Database database;
    public static String table = "\"orderline\"";
    // private static String join = "INNER JOIN " + Drink.table + " INNER JOIN " + Order.table;

    public static void init(Database database) {
        OrderLine.database = database;
    }

    public static OrderLine getByOrderAndDrink(Order order, Drink drink) {

        Cursor data = OrderLine.database.getReadable().rawQuery(
                "SELECT * FROM " + OrderLine.table + " WHERE order_id = ? AND drink_id = ?;",
                new String[]{Integer.toString(order.getId()), Integer.toString(drink.getId())}
        );
        data.moveToFirst();

        return OrderLine.craft(data);

    }

    public static void update(OrderLine orderLine) {

        ContentValues values = new ContentValues();

        values.put("quantity", orderLine.getQuantity());

        String whereClause = "order_id = ? AND drink_id = ?";

        OrderLine.database.getWritable().update(
                OrderLine.table,
                values,
                whereClause,
                new String[] {
                        Integer.toString(orderLine.getOrder().getId()),
                        Integer.toString(orderLine.getDrink().getId())
                }
        );

    }

    public static OrderLine[] getOrderLines(Order order) {
        return OrderLine.craftMultiple(database.getReadable()
                .rawQuery("SELECT * FROM " + table + " WHERE quantity > 0 AND order_id = ?;", new String[]{Integer.toString(order.getId())}));
    }

    private static OrderLine[] craftMultiple(Cursor data) {

        OrderLine[] orderlines = new OrderLine[data.getCount()];

        while (data.moveToNext()) {

            OrderLine orderLine = OrderLine.craft(data);
            if (orderlines != null) {
                orderlines[data.getPosition()] = orderLine;
            }
        }

        return orderlines;

    }

    private static OrderLine craft(Cursor data) {

        if (data.getCount() == 0)
            return null;


        // On va générer le drink et l'order associé
        int drink_id = data.getInt(data.getColumnIndex("drink_id"));
        int order_id = data.getInt(data.getColumnIndex("order_id"));
        int quantity = data.getInt(data.getColumnIndex("quantity"));

        return new OrderLine(
                Order.getById(order_id),
                Drink.getById(drink_id),
                quantity
        );
    }
}
