package net.infogroupeu.bartender.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.util.Log;

import net.infogroupeu.bartender.database.Database;

import java.io.IOException;
import java.io.InputStream;

public class Drink {

    private int id;
    private int icon;
    private String name;
    private String description;
    private float price;
    private String photo_path;
    private int stock;
    private int max;
    private int seuil;
    private String alcoolemie;
    private String quantite;

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public Drink(int id, int icon, String name, String description, float price, int stock, int max, int seuil, String alcoolemie, String quantite, String photo_path) {

        this.id = id;
            this.icon = icon;
            this.name = name;
        this.description = description;
        this.alcoolemie = alcoolemie;
        this.price = price;
        this.photo_path = photo_path;
        this.stock = stock;
        this.max = max;
        this.seuil = seuil;
        this.quantite = quantite;
    }

    public String getAlcoolemie() { return alcoolemie; }
    public void setAlcoolemie(String alcoolemie) { this.alcoolemie = alcoolemie;}

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public float getPrice() {
        return price;
    }

    public int getStock() { return stock; }
    public int getMax() { return max; }
    public int getSeuil() { return seuil; }

    public void setStock(int stock) {
        this.stock = stock;
        String whereClause = "boisson_id = ?";

        ContentValues values = new ContentValues();
        values.put("stock", stock);

        Drink.database.getWritable().update(
                Drink.table,
                values,
                whereClause,
                new String[] {
                        Integer.toString(getId())
                }
        );
    }

    public Drawable getPhoto(Context con) {
        if (this.photo_path == null || this.photo_path.isEmpty()) {
            return null;
        }
        try {
            InputStream photo_stream = con.getAssets().open(this.photo_path);
            return Drawable.createFromStream(photo_stream, null);
        }
        catch (IOException e) {
            Log.e("bartender","erreur IO",e);
            return null;
        }
    }
    public double getMeanNote(){

        Cursor data = Drink.database.getReadable()
                .rawQuery("SELECT AVG(note) FROM " + Vote.table + " WHERE boisson_id = ?;", new String[]{Integer.toString(this.id)});
        Log.i("database", Integer.toString(data.getCount()));
        if(data.getCount()==0){return 0.0;}
        data.moveToFirst();
        Log.i("database", Integer.toString(data.getCount()));
        return data.getDouble(0);

    }

    public Drawable getIcon(Context con) {

        try {
            InputStream photo_stream = con.getAssets().open("drinks/photos/logo_"+this.icon+".png");
            return Drawable.createFromStream(photo_stream, null);
        }
        catch (IOException e) {
            return null;
        }
    }
    /**
     * Static members
     */

    private static Database database;
    private static String table = "boisson";

    public static void init(Database database) {
        Drink.database = database;
    }

    public static Drink[] getAll() {

        return Drink.craftMultiple(Drink.database.getReadable()
                .rawQuery("SELECT * FROM " + Drink.table + ";", null));

    }

    public static Drink getById(int id) {
        Cursor data = Drink.database.getReadable()
                .rawQuery("SELECT * FROM " + Drink.table + " WHERE boisson_id = ?;", new String[]{Integer.toString(id)});
        data.moveToFirst();
        return Drink.craft(data);
    }

    public static Drink[] search(String query) {
        return Drink.craftMultiple(Drink.database.getReadable()
                .rawQuery("SELECT * FROM " + Drink.table + " WHERE nom LIKE '%"+query+"%' COLLATE utf8_general_ci;", null));
    }

    public static Drink[] craftMultiple(Cursor data) {

        Drink[] drinks = new Drink[data.getCount()];

        while (data.moveToNext()) {

            Drink drink = Drink.craft(data);
            if (drink != null) {
                drinks[data.getPosition()] = drink;
            }
        }

        return drinks;

    }

    public static Drink craft(Cursor data) {
        if (data.getCount()==0) {return null;}

        return new Drink(
                data.getInt(data.getColumnIndex("boisson_id")),
                data.getInt(data.getColumnIndex("icon")),
                data.getString(data.getColumnIndex("nom")),
                data.getString(data.getColumnIndex("description")),
                data.getFloat(data.getColumnIndex("prix")),
                data.getInt(data.getColumnIndex("stock")),
                data.getInt(data.getColumnIndex("max")),
                data.getInt(data.getColumnIndex("seuil")),
                data.getString(data.getColumnIndex("alcoolemie")),
                data.getString(data.getColumnIndex("quantite")),
                data.getString(data.getColumnIndex("photo"))

        );


    }

}

