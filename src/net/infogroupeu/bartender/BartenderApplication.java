package net.infogroupeu.bartender;

import android.app.Application;
import android.content.Context;
import net.infogroupeu.bartender.database.Database;
import net.infogroupeu.bartender.model.Drink;
import net.infogroupeu.bartender.model.Order;
import net.infogroupeu.bartender.model.OrderLine;
import net.infogroupeu.bartender.model.User;
import net.infogroupeu.bartender.model.Vote;

import java.util.Date;

public class BartenderApplication extends Application {

    private Database database;

    private User connectedUser;
    private Order currentOrder;

    private static BartenderApplication instance;

    public BartenderApplication() {

        BartenderApplication.instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.database = new Database(this, "bartender");

        Drink.init(this.database);
        User.init(this.database);
        Order.init(this.database);
        Vote.init(this.database);
        OrderLine.init(this.database);

    }

    public User loginUser(String login, String password) {

        User user = User.getUserByLogin(login);

        if (user == null) {
            return null;
        }

        if (password.equals(user.getPassword())) {

            // Connection !
            this.connectedUser = user;
            this.currentOrder = null;
            return user;

        } else {

            // Mauvais mot de passe
            return null;

        }

    }

    public Order beginOrder(int tableNumber) {

        // On va creer une commande
        User user = this.getConnectedUser();

        Order order = Order.create(user, tableNumber, new Date());

        this.currentOrder = order;

        return order;

    }

    public User getConnectedUser() {
        return connectedUser;
    }

    public Order getCurrentOrder() {

        if (this.currentOrder == null) {
            this.currentOrder = Order.getCurrentOrder(this.connectedUser);
        }

        return this.currentOrder;

    }

    public static BartenderApplication getInstance() {

        // Compact vs Lisible
        // return Application.instance == null ? new Application() : Application.instance;

        if (BartenderApplication.instance == null) {
            return new BartenderApplication();
        }

        return BartenderApplication.instance;
    }
}
