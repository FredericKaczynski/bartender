CREATE TABLE "boisson"(
    "boisson_id"  INTEGER PRIMARY KEY  NOT NULL  UNIQUE ,
    "icon"        INTEGER,
    "nom"         VARCHAR(50) NOT NULL  UNIQUE ,
    "prix"        DOUBLE NOT NULL ,
    "stock"       INTEGER,
    "max"         INTEGER NOT NULL,
    "seuil"       INTEGER NOT NULL,
    "alcoolemie"  DOUBLE,
    "quantite"    VARCHAR(10),
    "description" VARCHAR(200),
    "photo"       VARCHAR(50));

CREATE TABLE "user"(
    "user_id"  INTEGER  PRIMARY KEY  NOT NULL  UNIQUE,
    "login"    VARCHAR(18)  NOT NULL UNIQUE,
    "password" VARCHAR(18)  NOT NULL,
    "type"     INTEGER  NOT NULL);

CREATE TABLE "order"(
    "order_id"   INTEGER  PRIMARY KEY  NOT NULL,
    "date_create" BIGINT,
    "table_num"   INTEGER,
    "waiter_user_id"     INTEGER,
    "paid" INTEGER DEFAULT 0,
    FOREIGN KEY ("waiter_user_id") REFERENCES "user");

CREATE TABLE "orderline"(
    "order_id" INTEGER,
    "drink_id" INTEGER,
    "quantity" INTEGER,
    PRIMARY KEY ("order_id", "drink_id"));

CREATE TABLE "vote"(
    "boisson_id"  INTEGER  NOT NULL,
    "note"  DOUBLE,
    "user_id"  INTEGER  NOT NULL,
    PRIMARY KEY("boisson_id","user_id"));

INSERT INTO "boisson" VALUES (1, 1, "Eau", 1.0, 500, 500, 100, NULL,"33cl", "L’eau est un composé chimique ubiquitaire sur la Terre, essentiel pour tous les organismes vivants connus. C'est le milieu de vie de la plupart des êtres vivants. Elle se trouve en général dans son état liquide et possède à température ambiante des propriétés uniques : c’est notamment un solvant efficace pour beaucoup de corps solides trouvés sur Terre — l’eau est quelquefois désignée sous le nom de « solvant universel »." , "drinks/photos/eau.jpg");

INSERT INTO "boisson" VALUES (2, 1, "Coca", 1.0, 500, 500, 100, NULL,"33cl", "Le coca c'est délicieux mais en trop grande quantité c'est mauvais pour la santé !", "drinks/photos/coca.jpeg");

INSERT INTO "boisson" VALUES (3, 2, "Orval", 3.0, 100, 100, 20, 6.2, "33cl","Couleur mordorée-orangée, troublée. Mousse crémeuse, abondante. Assez peu saturée. Nez complexe, tons de fruits acides et amers (pomme, rhubarbe ?) Arôme mêlé de levure acre et de houblons aromatiques. Saveur extraordinaire, amertume sèche et intense, corps ample, velouté, légère acidité. Un must !", "drinks/photos/orval.jpg");

INSERT INTO "boisson" VALUES (4, 2, "Leffe blonde", 2.5, 200, 200, 50, 6.6, "33cl","Leffe Blonde est une authentique bière blonde d’Abbaye à la douce amertume qui se savoure à tout moment de la journée.", "drinks/photos/leffe.jpg");

INSERT INTO "boisson" VALUES (5, 2, "Jupiler", 2.0, 500, 500, 100, 5.2, "25cl","Jupiler est la marque de bière préférée des Belges depuis des dizaines d’années. Un Belge sur trois préfère la Jupiler et cette proportion est encore plus grande dans le groupe cible des 18-35 ans.", "drinks/photos/jup.jpg");

INSERT INTO "boisson" VALUES (6, 3, "Alamos Chardonnay 2013", 3.0, 500, 500, 100, 13.5,"25cl", "Vin produit par la famille Alamos provenant du célèbre cépage Chardonnay. Léger sucré et agréable à la fois, son caractère convient aussi bien aux apéros en plein air qu'aux fêtes bien arrosées. ", "drinks/photos/alamos.jpeg");

INSERT INTO "boisson" VALUES (7, 3, "Paolo Scavino Vino Rosso 2013", 3.2, 500, 500, 100, 13.5,"25cl","Avec le millésime 2013, Enrico Scavino en est à sa 63e vinification au domaine qui a été fondée en 1921 par son grand-père Paolo. Une expertise familiale établie qui se confirme dans le verre.","drinks/photos/vinorosso.png" );

INSERT INTO "boisson" VALUES (8, 3, "Perrier jouet brut 2006 (Champagne)", 25.0, 400, 100, 40, 12.0,"25cl", "Un vin floral, qui exprime parfaitement le style de la Maison Perrier-Jouët. Crée en 1856, elle a été la première maison à élaborer un champagne brut, en réduisant la quantité de sucres dans l’objectif de satisfaire ses clients anglais. ", "drinks/photos/perrier.jpg");

INSERT INTO "boisson" VALUES (9, 4, "Eristoff original (Vodka)", 3.5, 200, 200, 100, 37.5,"20cl", "Médaillé aux International Spirit's Challenge, l'eristoff blanche ne manquera pas de vous faire passer une soirée inoubliable (ou très oubliée) et de faire de vous le leader du groupe.","drinks/photos/vodka.jpg");

INSERT INTO "boisson" VALUES (10, 4, "Capitaine Morgan (Rhum foncé)", 4.0, 200, 300, 100, 40.0,"15cl", "Le rhum foncé Captain Morgan ® offre un goût riche et prononcé qui rappelle les « rations de rhum » offertes dans la marine et tant appréciées à la fois par les marins, les grands aventuriers et les connaisseurs.","drinks/photos/captain.jpeg");

INSERT INTO "boisson" VALUES (11, 2, "Kwak", 3.20, 100, 100, 20, 8.4,"33cl", "La Kwak est bière belge à la robe ambrée / cuivrée surmontée d'un grand col de mousse blanche. L'arôme est doux avec des senteurs de caramel, de levure, des notes de fruits, et d'épices. Sa saveur est douce avec des notes fruitées de prune et de noix associées à des pointes de malts caramel. Cela en fait une bière très douce et lisse, sans pour autant être trop sucrée. Cet ensemble rond est complété par une belle montée d'amertume tout au long de la dégustation, qui vient équilibrer le tout.","drinks/photos/kwak.jpg");

INSERT INTO "boisson" VALUES (12, 4, "Mojito maison", 6.0, 100, 100, 10, 7.5,"50cl", "Le mojito, produit sur place avec notre recette spéciale. Idéal lors des grosses chaleurs ou des soirées en terasse, le rhum est délicatement dilué avec la menthe et le jus de citron. Une fraicheur déroutante. ", "drinks/photos/mojito.jpg");

INSERT INTO "boisson" VALUES (13, 4, "Jack Daniels", 5.5, 200, 200, 100, 40.0,"20cl", "Le célèbre label du Tenessee ne vous décevra pas, que ce soit au niveau de sa teneur corsée mais légèrement sucrée ou de son arôme savoureusement boisé.", "drinks/photos/jack-daniels.jpg");

INSERT INTO "boisson" VALUES (14, 4, "Peket pomme", 2.0, 200, 200, 100, 17.5,"7cl", "Une petite douceur qui passe vite-fait bien-fait et vous donnera une pointe de tonus pour les prochaines minutes!", "drinks/photos/peket.png");

INSERT INTO "boisson" VALUES (15, 2, "La Verte du Mont Blanc", 2.0, 200, 200, 100, 5.9,"25cl", "Une bière brassé par la brasserie du mont blanc et aromatisée au génépi  ", "drinks/photos/LaVerte.jpeg");

INSERT INTO "user" VALUES (1, "owner1", "owner", 3);

INSERT INTO "user" VALUES (2, "waiter1", "waiter", 2);

INSERT INTO "user" VALUES (3, "client1", "client", 1);

INSERT INTO "user" VALUES (4, "thomas_w", "thomas", 2);

INSERT INTO "user" VALUES (5, "fred_k", "fred", 2);

INSERT INTO "user" VALUES (6, "kenzo_f", "kenzo", 2);

INSERT INTO "user" VALUES (7, "olivier_a", "olivier", 2);

INSERT INTO "user" VALUES (8, "mathieu_x", "mathieu", 2);

INSERT INTO "user" VALUES (9, "nico_l", "nico", 2);

INSERT INTO "user" VALUES (10, "kim_m", "kim", 1);

INSERT INTO "user" VALUES (11, "simeon_m", "simeon", 2);

INSERT INTO "user" VALUES (12, "patrick", "patrick", 1);

INSERT INTO "user" VALUES (13, "clara", "clara", 1);

INSERT INTO "user" VALUES (14, "julian", "julian", 1);

INSERT INTO "user" VALUES (15, "jeanne", "jeanne", 1);

INSERT INTO "user" VALUES (16, "catherine_r", "catherine", 1);

INSERT INTO "user" VALUES (17, "paris_h", "paris", 1);

INSERT INTO "user" VALUES (18, "kim_k", "kim_k", 1);

INSERT INTO "vote" VALUES (2, 4.7, 1);

INSERT INTO "vote" VALUES (2, 3.7, 2);

INSERT INTO "vote" VALUES (2, 4.7, 3);

INSERT INTO "vote" VALUES (2, 4.7, 4);

INSERT INTO "vote" VALUES (2, 4.7, 5);

INSERT INTO "vote" VALUES (2, 4.7, 6);

INSERT INTO "vote" VALUES (2, 3.7, 7);

INSERT INTO "vote" VALUES (2, 1.7, 8);

INSERT INTO "vote" VALUES (2, 1.7, 9);

INSERT INTO "vote" VALUES (2, 2.7, 10);

INSERT INTO "vote" VALUES (2, 4.7, 11);

INSERT INTO "vote" VALUES (2, 4.7, 12);

INSERT INTO "vote" VALUES (2, 4.7, 13);

INSERT INTO "vote" VALUES (2, 4.7, 14);

INSERT INTO "vote" VALUES (2, 4.7, 15);

INSERT INTO "vote" VALUES (2, 3.7, 16);

INSERT INTO "vote" VALUES (2, 4.7, 17);

INSERT INTO "vote" VALUES (2, 4.7, 18);

INSERT INTO "vote" VALUES (3, 5.0, 1);

INSERT INTO "vote" VALUES (4, 4.0, 2);

INSERT INTO "vote" VALUES (5, 4.5, 3);

INSERT INTO "vote" VALUES (6, 3.5, 1);

INSERT INTO "vote" VALUES (7, 4.5, 2);

INSERT INTO "order" VALUES (1, 1431099000000, 1, 2, 0);

INSERT INTO "order" VALUES (2, 1431014400000, 2, 11, 0);

INSERT INTO "order" VALUES (3, 1430929800000, 3, 4, 0);

INSERT INTO "order" VALUES (4, 1430845200000, 4, 7, 0);

INSERT INTO "order" VALUES (5, 1430760600000, 5, 6, 0);

INSERT INTO "order" VALUES (6, 1430676000000, 1, 11, 0);

INSERT INTO "order" VALUES (7, 1430591400000, 2, 11, 1);

INSERT INTO "order" VALUES (8, 1430506800000, 3, 11, 1);

INSERT INTO "order" VALUES (9, 1430422200000, 4, 4, 1);

INSERT INTO "order" VALUES (10, 1431099000000, 5, 7, 1);

INSERT INTO "order" VALUES (11, 1430337600000, 1, 11, 1);

INSERT INTO "order" VALUES (12, 1430253000000, 2, 6, 1);

INSERT INTO "order" VALUES (13, 1430168400000, 3, 9, 1);

INSERT INTO "order" VALUES (14, 1430083800000, 4, 11, 1);

INSERT INTO "order" VALUES (15, 1429999200000, 5, 11, 1);

INSERT INTO "order" VALUES (16, 1429914600000, 1, 6, 0);

INSERT INTO "order" VALUES (17, 1429830000000, 2, 7, 1);

INSERT INTO "order" VALUES (18, 1429745400000, 3, 8, 1);

INSERT INTO "order" VALUES (19, 1429660800000, 4, 11, 1);

INSERT INTO "order" VALUES (20, 1429576200000, 5, 11, 1);

INSERT INTO "order" VALUES (21, 1429491600000, 1, 8, 1);

INSERT INTO "order" VALUES (22, 1429407000000, 2, 7, 1);

INSERT INTO "order" VALUES (23, 1429322400000, 3, 5, 1);

INSERT INTO "order" VALUES (24, 1429237800000, 4, 6, 1);

INSERT INTO "order" VALUES (25, 1429153200000, 5, 11, 1);

INSERT INTO "order" VALUES (26, 142906860000, 1, 11, 1);

INSERT INTO "order" VALUES (27, 1428984000000, 2, 8, 0);

INSERT INTO "order" VALUES (28, 1428899400000, 3, 7, 1);

INSERT INTO "order" VALUES (29, 1428814800000, 4, 8, 0);

INSERT INTO "order" VALUES (30, 1428730200000, 5, 11, 1);

INSERT INTO "orderline" VALUES (1, 3 , 15);

INSERT INTO "orderline" VALUES (2, 3 , 5);

INSERT INTO "orderline" VALUES (3, 2 , 6);

INSERT INTO "orderline" VALUES (4, 2 , 8);

INSERT INTO "orderline" VALUES (5, 4 , 15);

INSERT INTO "orderline" VALUES (6, 4 , 20);

INSERT INTO "orderline" VALUES (7, 4 , 17);

INSERT INTO "orderline" VALUES (8, 5 , 20);

INSERT INTO "orderline" VALUES (9, 12, 30);

INSERT INTO "orderline" VALUES (10, 15 , 35);

INSERT INTO "orderline" VALUES (11, 15 , 10);

INSERT INTO "orderline" VALUES (12, 11 , 5);

INSERT INTO "orderline" VALUES (13, 7 , 7);

INSERT INTO "orderline" VALUES (14, 7 , 8);

INSERT INTO "orderline" VALUES (15, 8 , 15);

INSERT INTO "orderline" VALUES (16, 14 , 10);

INSERT INTO "orderline" VALUES (17, 13 , 10);

INSERT INTO "orderline" VALUES (18, 11 ,10);

INSERT INTO "orderline" VALUES (19, 11 , 13);

INSERT INTO "orderline" VALUES (20, 12 , 16);

INSERT INTO "orderline" VALUES (21, 12 , 19);

INSERT INTO "orderline" VALUES (22, 13 , 3);

INSERT INTO "orderline" VALUES (23, 15 , 2);

INSERT INTO "orderline" VALUES (24, 7 , 5);

INSERT INTO "orderline" VALUES (25, 6 , 10);

INSERT INTO "orderline" VALUES (26, 5 , 15);

INSERT INTO "orderline" VALUES (27, 4 , 17);

INSERT INTO "orderline" VALUES (28, 4 , 18);

INSERT INTO "orderline" VALUES (29, 7 , 19);

INSERT INTO "orderline" VALUES (30, 9 , 20);

INSERT INTO "orderline" VALUES (1, 4, 30);

INSERT INTO "orderline" VALUES (2, 4 , 18);

INSERT INTO "orderline" VALUES (2, 5 , 14);

INSERT INTO "orderline" VALUES (3, 9 , 7);

INSERT INTO "orderline" VALUES (3, 15 , 6);

INSERT INTO "orderline" VALUES (4, 11 , 15);

INSERT INTO "orderline" VALUES (4, 9 , 3);

INSERT INTO "orderline" VALUES (4, 8 , 2);

INSERT INTO "orderline" VALUES (5, 7 , 4);

INSERT INTO "orderline" VALUES (5, 6 , 5);

INSERT INTO "orderline" VALUES (7, 5 , 7);

INSERT INTO "orderline" VALUES (8, 3 , 12);

INSERT INTO "orderline" VALUES (8, 6 , 13);

INSERT INTO "orderline" VALUES (8, 8 , 12);

INSERT INTO "orderline" VALUES (8, 10 , 14);

INSERT INTO "orderline" VALUES (9, 11 , 17);

INSERT INTO "orderline" VALUES (10, 14 , 1);

INSERT INTO "orderline" VALUES (11, 16 , 2);

INSERT INTO "orderline" VALUES (12, 12 , 3);

INSERT INTO "orderline" VALUES (12, 10 , 19);

INSERT INTO "orderline" VALUES (13, 9 , 8);

INSERT INTO "orderline" VALUES (14, 6 , 7);

INSERT INTO "orderline" VALUES (15, 6 , 6);

INSERT INTO "orderline" VALUES (15, 9 , 5);

INSERT INTO "orderline" VALUES (16, 11 , 4);
